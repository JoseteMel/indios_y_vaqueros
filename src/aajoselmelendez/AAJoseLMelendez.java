package aajoselmelendez;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author José Luis Meléndez Mateos
 * @title Actividad de aprendizaje (1ª Eval. Programación)
 * 
 * ENUNCIADO:
 *      Vamos a realizar un juego de tablero que permita jugar por turnos a dos 
 *      jugadores.
 * 
 * Configurar:
 *      1. Representa dos tableros de juego (2 jugadores) mediante una matriz de
 *      caracteres de 5x5.
 * 
 *      2. Sitúa al jugador de cada tablero de forma aleatoria y represéntalos 
 *      con el carácter ‘I’ (jugador 1, indio) y ‘V’ (jugador 2, vaquero),
 *      respectivamente.
 * 
 *           Rellena aleatoriamente con 5 vaqueros ‘V’ el tablero con el que 
 *          jugará el indio ‘I’.
 * 
 *           Rellena aleatoriamente con 5 indios ‘I’ el tablero con el que 
 *          jugará el vaquero ‘V’.
 * 
 *      3. Las celdas que no estén ocupadas por vaqueros/indios de cada tablero
 *      y por el jugador correspondiente, quedarán representadas con la letra
 *      ‘L’.
 * 
 *      4. Cada jugador tiene 3 vidas, si cae en una casilla donde se encuentra 
 *      un vaquero/indio ‘I o V’, perderá una vida.
 * 
 *      5. Pierde el juego el jugador que antes pierda las 3 vidas.
 * 
 * Jugar:
 *      1. Cada jugador tiene un único turno, a excepción de que esté 
 *      incumpliendo el límite de tablero.
 *      
 *           En este caso podrá volver a desplazarse.
 * 
 *           Pasamos al turno siguiente cuando perdamos una vida o nos 
 *          desplacemos a una casilla “Libre”.
 *      
 *      2. El jugador 1 (Indio), se desplazará sobre su tablero 1, y el 
 *      jugador 2 (Vaquero), se desplazará sobre su tablero 2.
 *      
 *      3. El jugador podrá pulsar cuatro teclas. Cada una de ellas le 
 *      proporcionará un movimiento: derecha, izquierda, arriba y abajo 
 *      (‘D’, ‘A’,’W’ y ‘S’).
 * 
 *      4. Prepara el juego para que no exista límite de tablero.
 *
 *           Cuando un jugador, en cualquier dirección, se vaya a salir del 
 *          tablero, programaremos el juego para que sea capaz de aparecer por 
 *          el lado contrario, como si rodeara el tablero.
 * 
 *      5. Desarrollar un algoritmo que permita al jugador moverse en las 
 *      casillas libres “L”.
 * 
 *           Si se encuentra con una celda ocupada por un vaquero/indio 
 *          “V o I”, el jugador pierde una vida.
 * 
 *           Si pierde las 3 vidas, se terminó el juego y ha ganado el otro 
 *          jugador.
 * 
 * Recomendación:
 *       Deja visibles los tableros para que observes la funcionalidad.
 * 
 */
public class AAJoseLMelendez {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);    // Lee datos del teclado
        
        /******* CONSTANTES *******/
        char LIBRE = '*';           // Icono de las casillas libres en el tablero
        char INDIO = 'I';           // Icono de los indios en el tablero
        char VAQUERO = 'V';         // Icono de los vaqueros en el tablero
        
        String INDIO_NOMBRE = "INDIO";      // Nombre del jugador indio
        String VAQUERO_NOMBRE = "VAQUERO";  // Nombre del jugador vaquero
        
        int FILAS = 6;              // Establece el número de filas del tablero
        int COLUMNAS = 6;           // Establece el número de columnas del tablero
        
        int VIDAS = 3;              // Establece el número de vidas de los jugadores
        
        int NUM_ENEMIGOS = 5;       // Establece el número de enemigos en el tablero
        
        int RONDAS_MAX = 999999;    // [Configuración] Establece un máximo de rondas

        
        /******* VARIABLES *******/
        int vidasIndio = VIDAS;     // Asigna un número de vidas para el Indio
        int vidasVaquero = VIDAS;   // Asigna un número de vidas para el Vaquero

        int indioFila = 0;          // Contiene la posición del indio en las filas
        int indioCol = 0;           // Contiene la posición del indio en las columnas

        int vaqueroFila = 0;        // Contiene la posición del vaquero en las filas
        int vaqueroCol = 0;         // Contiene la posición del vaquero en las columnas
        
        int contador;           // Se usa para controlar el funcionamiento interno del código
        
//        int dificultad = 0;         // [Configuración] Establece una dificultad al juego
        int rondas = 1;             // Cuenta las rondas que se han jugado
        
        String respuesta = " ";     // [Configuración] Toma de decisión en la configuración

        String movJugador;    // Establece la dirección del movimiento del jugador

        int[] infoJugador;    // Se usa para extraer valores de las funciones
        
        
        /*********** MENÚ INICIAL ************/
        System.out.println("_-_-_-_ I N D I O S    Y    V A Q U E R O S _-_-_-_");
        System.out.println("");
        System.out.println("============ BIENVENIDO ============");
        
        do {        // Si no quieres configurar el juego, salta a la línea 241              <<<<<<<<<<<-----------------
        System.out.print("¿Quieres configurar el juego? (S/N): ");
        respuesta = sc.nextLine();
        } while (!respuesta.equalsIgnoreCase("S") && !respuesta.equalsIgnoreCase("N"));
        System.out.println("");
        
        if (respuesta.equalsIgnoreCase("S")) {
            System.out.println("========= CONFIGURACIÓN =========");
            
// [Esta función aun está en desarrollo] Según la dificultad escogida, el número
// de vidas y el tamaño del tablero, más adelante acotará al usuario la cantidad
// de enemigos que se podrán poner en el tablero
//
//            do {    // Solo permite elegir entre el nivel 1 y 5
//            System.out.println("Elige un nivel de dificultad del 1 al 5.");
//            System.out.print("  (1 fácil - 5 difícil): ");
//            dificultad = sc.nextInt();
//            } while (dificultad <1 || dificultad >5);
//            
//           
//            sc.nextLine();      // Limpia el buffer de recogida de datos (String)

            
            // Permite elegir el nombre de los jugadores y su icono en el tablero
            System.out.print("Elije el nombre del primer jugador (INDIO): ");
            INDIO_NOMBRE = sc.nextLine().toUpperCase();
            
            System.out.print("Elige el icono del jugador " + INDIO_NOMBRE + ": ");
            INDIO = sc.next().toUpperCase().charAt(0);
            
            sc.nextLine();      // Limpia el buffer de recogida de datos (String)

            System.out.print("Elije el nombre del segundo jugador (VAQUERO): ");
            VAQUERO_NOMBRE = sc.nextLine().toUpperCase();
            
            contador = 0;       // Resetea el contador a 0
            
            do {                // No permite elegir el mismo icono que el Indio
                if (contador != 0){
                    System.out.println("No puede ser igual que el de " + INDIO_NOMBRE +".");
                }
            System.out.print("Elige el icono del jugador " + VAQUERO_NOMBRE + ": ");
            VAQUERO = sc.next().toUpperCase().charAt(0);
            contador++;
            } while (VAQUERO == INDIO);
            
            
            // Cómo se mostrarán las casillas libres
            contador = 0;       // Resetea el contador a 0
            
            do {                // No permite elegir el mismo icono que los jugadores
                if (contador != 0) {
                    System.out.println("No puede ser igual que el de los jugadores.");
                }
            System.out.print("Elige el icono para las casillas LIBRES: ");
            LIBRE = sc.next().toUpperCase().charAt(0);
            contador++;
            } while (LIBRE == INDIO || LIBRE == VAQUERO);
            
            
            // Elige el número máximo de vidas que tendrán los jugadores
            do {
            System.out.print("Elige el número de vidas (mínimo 1): ");
            VIDAS = sc.nextInt();
            vidasIndio = VIDAS;
            vidasVaquero = VIDAS;
            } while (VIDAS < 1);
            
            
            // Permite elegir el tamaño del tablero con mínimo 3x3
            do {
            System.out.println("Elige un tamaño para el tablero (min 3x3).");
            System.out.print("  Filas: ");
            FILAS = sc.nextInt();
            System.out.print("  Columnas: ");
            COLUMNAS = sc.nextInt();
            } while (FILAS < 3 || COLUMNAS < 3);
            
            
            /** Elige el número de enemigos y muestra una recomendación según 
             * el nivel elegido */
            contador = 0;           // Resetea el contador a 0

            do {
                if (contador != 0) {
                    System.out.println("Mínimo 3 enemigos y máximo " +
                            (FILAS * COLUMNAS / 2));
                }
                System.out.print("Elige el número de enemigos (entre 3 y "
                        + (FILAS * COLUMNAS / 2) + "): ");
            NUM_ENEMIGOS = sc.nextInt();
            contador++;
            } while (NUM_ENEMIGOS < 3 || NUM_ENEMIGOS > FILAS * COLUMNAS / 2);
            
            
            sc.nextLine();      // Limpia el buffer de recogida de datos (String)
            System.out.println("");
            
            // Configura un máximo de rondas a jugar antes de finalizar
            do {
                System.out.print("¿Quieres establecer un número máximo de"
                        + " rondas? (S/N): ");
                respuesta = sc.nextLine();
            } while (!respuesta.equalsIgnoreCase("S") && !respuesta.equalsIgnoreCase("N"));
            
            if (respuesta.equalsIgnoreCase("S")) {
                do {    // Exige elegir un mínimo de 5 rondas
                System.out.print("¿Cuántas rondas quieres que tenga el juego?"
                        + " (Mínimo 5): ");
                    RONDAS_MAX = sc.nextInt();
                } while (RONDAS_MAX < 5);
            }
            
            System.out.println("Enhorabuena, el juego está configurado. Comencemos...");
            System.out.println("");
        } else if (respuesta.equalsIgnoreCase("N")) {
            System.out.println("Comencemos...");
            System.out.println("");
        }
        
        contador = 0;               // Resetea el contador a 0
        
        // Easter egg navideño :)
        switch (INDIO_NOMBRE) {
            case "MELCHOR":
            case "GASPAR":
            case "BALTASAR":
            case "PAPA NOEL":
                easterEggNavidad();
                contador++;
            break;
        }
        
        switch (VAQUERO_NOMBRE) {
            case "MELCHOR":
            case "GASPAR":
            case "BALTASAR":
            case "PAPA NOEL":
                if (contador == 0) {    // Asegura que solo se muestra un árbol
                    easterEggNavidad();
                }
            break;
        }
        
        /******* CREO LOS TABLEROS *******/
        char[][] tableroIndioSolucion = new char[FILAS][COLUMNAS];
        char[][] tableroVaqueroSolucion = new char[FILAS][COLUMNAS];
        char[][] tableroIndioJugador = new char[FILAS][COLUMNAS];
        char[][] tableroVaqueroJugador = new char[FILAS][COLUMNAS];

        
        /******* LLENO LOS TABLEROS *******/
        /* Lleno las casillas con "L"*/
        for (int i = 0; i < FILAS; i++) {
            for (int j = 0; j < COLUMNAS; j++) {
                tableroIndioSolucion[i][j] = LIBRE;
                tableroVaqueroSolucion[i][j] = LIBRE;
                tableroIndioJugador[i][j] = LIBRE;
                tableroVaqueroJugador[i][j] = LIBRE;
            }
        }

        
        /* Coloco a los enemigos en los tableros */
        
        generarEnemigos(NUM_ENEMIGOS, FILAS, COLUMNAS, tableroIndioSolucion,
                LIBRE, VAQUERO);
        
        generarEnemigos(NUM_ENEMIGOS, FILAS, COLUMNAS, tableroVaqueroSolucion,
                LIBRE, INDIO);

        
        /* Coloco a los jugadores en sus respectivos tableros */
        
        // TABLERO DEL INDIO
        infoJugador = colocarJugador(indioFila, indioCol, FILAS,
                COLUMNAS, tableroIndioSolucion, tableroIndioJugador, LIBRE, INDIO);
        // Actualizo las variables de posición con los datos devueltos por la función
        indioFila = infoJugador[0];
        indioCol = infoJugador[1];

        // TABLERO DEL VAQUERO
        infoJugador = colocarJugador(vaqueroFila, vaqueroCol, FILAS,
                COLUMNAS, tableroVaqueroSolucion, tableroVaqueroJugador, LIBRE, VAQUERO);
        // Actualizo las variables de posición con los datos devueltos por la función
        vaqueroFila = infoJugador[0];
        vaqueroCol = infoJugador[1];
        
        
        /******* MUESTRO LOS TABLEROS Y COMIENZA EL JUEGO *******/
        
        System.out.println("=============== JUEGO ===============");
        System.out.println("");
        
        do {
            // Muestra el número de ronda que se está jugando
            System.out.println("          RONDA: " + rondas);
            System.out.println("");
            
            // TURNO DEL INDIO
            System.out.println("------ TURNO DE: " + INDIO_NOMBRE + " ------");
            System.out.println("--- (Vidas restantes: " + vidasIndio + ") ---");
            
            /***(TABLERO DEL DESARROLLADOR) ***/
            mostrarTablero(tableroIndioSolucion, FILAS, COLUMNAS);
            System.out.println("");
            
//            /*** (TABLERO DDEL JUGADOR) ***/
//            mostrarTablero(tableroIndioJugador, FILAS, COLUMNAS);
//            System.out.println("");

            /* Pido al jugador que introduzca la tecla para moverse */
            movJugador = pedirMovimiento();

            /* Realizo los movimientos W A S D */
            infoJugador = moverJugador(movJugador, tableroIndioSolucion,
                    tableroIndioJugador, FILAS, COLUMNAS, indioFila, indioCol,
                    LIBRE, INDIO, VAQUERO, vidasIndio);
            // Actualizo la posición del jugador y sus vidas en las variables del main
            indioFila = infoJugador[0];
            indioCol = infoJugador[1];
            vidasIndio = infoJugador[2];
            
            // Muestro el tablero de nuevo y si ha perdido vidas con el movimiento
            mostrarTablero(tableroIndioSolucion, FILAS, COLUMNAS); // Tablero desarrollador
//            mostrarTablero(tableroIndioJugador, FILAS, COLUMNAS); // Tablero jugador
            
            mensajeMovimiento();
            

            // TURNO DEL VAQUERO
            if (vidasIndio != 0) {  // Antes de dejar jugar al vaquero, comprueba que el indio sigue vivo
                System.out.println("------ TURNO DE: " + VAQUERO_NOMBRE + " ------");
                System.out.println("--- (Vidas restantes: " + vidasVaquero + ") ---");
                
                /***(TABLERO DEL DESARROLLADOR) ***/
                mostrarTablero(tableroVaqueroSolucion, FILAS, COLUMNAS);
                System.out.println("");
                
//                /***(TABLERO DEL JUGADOR) ***/
//                mostrarTablero(tableroVaqueroJugador, FILAS, COLUMNAS);
//                System.out.println("");

                /* Pido al jugador que introduzca la tecla para moverse */
                movJugador = pedirMovimiento();

                /* Realizo los movimientos W A S D */
                infoJugador = moverJugador(movJugador, tableroVaqueroSolucion,
                        tableroVaqueroJugador, FILAS, COLUMNAS, vaqueroFila,
                        vaqueroCol, LIBRE, VAQUERO, INDIO, vidasVaquero);
                // Actualizo la posición del jugador y sus vidas en las variables del main
                vaqueroFila = infoJugador[0];
                vaqueroCol = infoJugador[1];
                vidasVaquero = infoJugador[2];
                
                // Muestro el tablero de nuevo y si ha perdido vidas con el movimiento
                mostrarTablero(tableroVaqueroSolucion, FILAS, COLUMNAS); // Tablero desarrollador
//                mostrarTablero(tableroVaqueroJugador, FILAS, COLUMNAS); // Tablero jugador
                
                mensajeMovimiento();
            }
            
            rondas++;
            
        } while (vidasIndio > 0 && vidasVaquero > 0 && rondas <= RONDAS_MAX);

        
        /******* RESULTADO *******/
        System.out.println("");
        System.out.println("_-_-_-_- FIN DEL JUEGO -_-_-_-_");
        System.out.println("");
        if (vidasIndio == 0) {  // Si el indio pierde
            System.out.println("Ha ganado el jugador: " + VAQUERO_NOMBRE + ".");
        } else if (vidasVaquero == 0) {     // Si el vaquero pierde
            System.out.println("Ha ganado el jugador: " + INDIO_NOMBRE + ".");
        } else if (rondas > RONDAS_MAX) {   // Si se acaban las rondas
            System.out.println("Se han acabado las rondas.");
            if (vidasIndio > vidasVaquero) {    // Si se acaban las rondas y el indio tiene más vidas
                System.out.println("Ha ganado el jugador: " + INDIO_NOMBRE + ".");
            } else if (vidasIndio < vidasVaquero) { // Si se acaban las rondas y el vaquero tiene más vidas
                System.out.println("Ha ganado el jugador: " + VAQUERO_NOMBRE + ".");
            } else if (vidasIndio == vidasVaquero) {    // Si se acaban las rondas y tienen las mismas vidas
                System.out.println(INDIO_NOMBRE + " y " + VAQUERO_NOMBRE + " han empatado.");
            }
        }
    }
    
    public static void generarEnemigos(int numEnemigos, int filas, int columnas, char[][] tablero,
            char libre, char enemigo){
        
        /*
         * Esta función coloca los enemigos en las tablas de los jugadores
        */
        
        Random posicionRandom = new Random();   // Genera números aleatorios

        int xAleatorio = 0;
        int yAleatorio = 0;
        
        for (int i = 0; i < numEnemigos; i++) {            
            
            do {        // Comprueba si esa casilla está libre
                xAleatorio = posicionRandom.nextInt(filas);
                yAleatorio = posicionRandom.nextInt(columnas);
            } while (tablero[xAleatorio][yAleatorio] != libre);
            tablero[xAleatorio][yAleatorio] = enemigo;
        }
    }
    
    public static void mostrarTablero(char[][] tablero, int filas, int columnas) {
        
        /*
         * Esta función sirve para mostrar los tableros por pantalla
        */
        
        System.out.println("");
            for (int i = 0; i < filas; i++) {
                for (int j = 0; j < columnas; j++) {
                    System.out.print(tablero[i][j] + "  ");
                }
                System.out.println("");
            }
    }
        
    public static int[] colocarJugador(int jugFila, int jugCol, int filas, 
            int columnas, char[][] tablero, char[][] tableroJugador, char libre,
            char jugador) {
        
        /*
         * Coloca al jugador en el tablero y devuelve los valores de su posición
         * a las variables del main
        */
        
        Random posicionRandom = new Random();   // Genera números aleatorios
        
        do {        // Comprueba si esa casilla está libre
            jugFila = posicionRandom.nextInt(filas);
            jugCol = posicionRandom.nextInt(columnas);
        } while (tablero[jugFila][jugCol] != libre);
        
        tablero[jugFila][jugCol] = jugador;
        tableroJugador[jugFila][jugCol] = jugador;
        
        // Devuelvo la nueva posición al main con este array
        int[] posicionJugador = {jugFila, jugCol};
        
        return posicionJugador;
    }
    
    public static String pedirMovimiento() {
        
        /*
         * Sirve para solicitar al jugador un movimiento con las teclas W A S D
        */
        
        Scanner sc = new Scanner(System.in);    // Lee datos del teclado
        
        int contador = 0;
        String movJugador = " ";
        
        do {
            if (contador != 0) { // Si introduce una tecla que no sea W A S D
                System.out.println("Movimiento incorrecto.");
                System.out.println("");
            }
            contador++;
            System.out.print("Pulsa una tecla para moverte (W A S D): ");
            movJugador = sc.nextLine();
        } while (!movJugador.equalsIgnoreCase("W") &&
                    !movJugador.equalsIgnoreCase("A") &&
                    !movJugador.equalsIgnoreCase("S") &&
                    !movJugador.equalsIgnoreCase("D"));
        
        // De vuelvo al main la tecla que ha escogido el jugador para moverse
        return movJugador;
    }
    
    public static int[] moverJugador(String movJugador, char[][] tableroSolucion,
            char[][] tableroJugador, int filas, int columnas, int jugFila,
            int jugCol, char libre, char jugador, char enemigo, int vidas) {
        
        /*
         * Controla el movimiento del jugador por el tablero y todos sus
         * posibles eventos en el transcurso, como restarle vidas al encontrarse
         * con un enemigo o cruzar el tablero de un lado al otro si éste se
         * encuentra en un borde
        */
        
        int contador = 0;
        
        switch (movJugador.toUpperCase()) {
                case "W":
                    if (jugFila == 0) { // Si el jugador está en el borde superior
                        if (tableroSolucion[filas - 1][jugCol] == libre) {
                            tableroSolucion[filas - 1][jugCol] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[filas - 1][jugCol] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                        } else if (tableroSolucion[filas - 1][jugCol] == enemigo) {
                            tableroSolucion[filas - 1][jugCol] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[filas - 1][jugCol] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                            vidas--;
                            contador++;
                        }
                        jugFila = filas - 1;
                    } else if (tableroSolucion[jugFila - 1][jugCol] == libre) {
                        tableroSolucion[jugFila - 1][jugCol] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila - 1][jugCol] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugFila--;
                    } else if (tableroSolucion[jugFila - 1][jugCol] == enemigo) {
                        tableroSolucion[jugFila - 1][jugCol] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila - 1][jugCol] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugFila--;
                        vidas--;
                        contador++;
                    }
                    break;
                case "A":
                    if (jugCol == 0) {  // Si el jugador está en el borde izquierdo
                        if (tableroSolucion[jugFila][columnas - 1] == libre) {
                            tableroSolucion[jugFila][columnas - 1] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[jugFila][columnas - 1] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                        } else if (tableroSolucion[jugFila][columnas - 1] == enemigo) {
                            tableroSolucion[jugFila][columnas - 1] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[jugFila][columnas - 1] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                            vidas--;
                            contador++;
                        }
                        jugCol = columnas - 1;

                    } else if (tableroSolucion[jugFila][jugCol - 1] == libre) {
                        tableroSolucion[jugFila][jugCol - 1] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila][jugCol - 1] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugCol--;
                    } else if (tableroSolucion[jugFila][jugCol - 1] == enemigo) {
                        tableroSolucion[jugFila][jugCol - 1] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila][jugCol - 1] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugCol--;
                        vidas--;
                        contador++;
                    }
                    break;
                case "S":
                    if (jugFila == filas - 1) { // Si el jugador está en el borde inferior
                        if (tableroSolucion[0][jugCol] == libre) {
                            tableroSolucion[0][jugCol] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[0][jugCol] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                        } else if (tableroSolucion[0][jugCol] == enemigo) {
                            tableroSolucion[0][jugCol] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[0][jugCol] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                            vidas--;
                            contador++;
                        }
                        jugFila = 0;
                    } else if (tableroSolucion[jugFila + 1][jugCol] == libre) {
                        tableroSolucion[jugFila + 1][jugCol] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila + 1][jugCol] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugFila++;
                    } else if (tableroSolucion[jugFila + 1][jugCol] == enemigo) {
                        tableroSolucion[jugFila + 1][jugCol] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila + 1][jugCol] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugFila++;
                        vidas--;
                        contador++;
                    }
                    break;
                case "D":
                    if (jugCol == columnas - 1) {   // Si el jugador está en el borde derecho
                        if (tableroSolucion[jugFila][0] == libre) {
                            tableroSolucion[jugFila][0] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[jugFila][0] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                        } else if (tableroSolucion[jugFila][0] == enemigo) {
                            tableroSolucion[jugFila][0] = jugador;
                            tableroSolucion[jugFila][jugCol] = libre;
                            tableroJugador[jugFila][0] = jugador;
                            tableroJugador[jugFila][jugCol] = libre;
                           vidas--;
                           contador++;
                        }
                        jugCol = 0;
                    } else if(tableroSolucion[jugFila][jugCol + 1] == libre) {
                        tableroSolucion[jugFila][jugCol + 1] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila][jugCol + 1] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugCol++;
                    } else if (tableroSolucion[jugFila][jugCol + 1] == enemigo) {
                        tableroSolucion[jugFila][jugCol + 1] = jugador;
                        tableroSolucion[jugFila][jugCol] = libre;
                        tableroJugador[jugFila][jugCol + 1] = jugador;
                        tableroJugador[jugFila][jugCol] = libre;
                        jugCol++;
                        vidas--;
                        contador++;
                    }
                    break;
        }
        
        if (contador != 0) {    // Mensaje si se encuentra con un enemigo y pierde una vida
            System.out.println("");
            System.out.println("Te has encontrado con un enemigo.");
            System.out.println("--- Ahora te quedan " + vidas + " vidas ---");
        } else {    // Mensaje si va a una casilla libre
            System.out.println("");
            System.out.println("Sigues avanzando por el tablero.");
            System.out.println("--- Todavía te quedan " + vidas + " vidas ---");
        }
        
        
        // Devuelvo la nueva posición del jugador y sus vidas al main
        int[] infoJugador = {jugFila, jugCol, vidas};
        
        return infoJugador;
    }
    
    public static void mensajeMovimiento() {
        
        /*
         * Muestra este mensaje después de que el jugador realice un movimiento
        */
        
        Scanner sc = new Scanner(System.in);    // Lee datos del teclado

        System.out.println("");
        System.out.println("(Pulsa intro para continuar...)");
        sc.nextLine();
        
        System.out.println("");
        System.out.println("======================================");

    }
    
    public static void easterEggNavidad() {
        
        /** 
         * Este easter egg solo se ejecuta si configuras el juego y el jugador
         * indio se llama como uno de los reyes magos ("Melchor", "Gaspar" o 
         * "Baltasar") o si se llama "Papa Noel".
        */
        
        int i, j, k, n;
        
        n = 10;
        
        System.out.println("        FELIZ NAVIDAD");
        System.out.println("");
        
        for (i = 1; i < n + (n / 2); i++) {
            for (j = n + (n / 2); j > i; j--) {
                System.out.print(" ");
            }
            for (k = 1; k <= 2 * i - 1; k++) {
                System.out.print("*");
            }
            System.out.println("");
        }
        
        for (i = 1; i < n - (n / 2); i++) {
            for (j = n + (n / 2); j > 1; j--) {
                System.out.print(" ");
            }
            for (k = n / 2; k <= (n / 2) + 1; k++) {
                System.out.print("*");
            }
            System.out.println("");
        }
        
        System.out.println("");
        System.out.println("Ahora sí, disfruta de la partida :D");
        System.out.println("    - @josetemel");
        System.out.println("");
    }
}
